# pip3 install numpy
# pip3 install Pillow

from classes.cube import Cube
from classes.piece import Piece
from classes.sens import Sens
import numpy as np

print("Solver du cube 25Y")
# Principe (Recursive Backtracking) :
# 1. On prend une pièce n.
# 2. On la place dans la première position disponible dans le cube.
# 3. On la place dans le premier sens possible.
# 4. SI aucun sens possible, on retourne à 2. et on prend la position suivante
# 5. SI aucune position ne passe, on retourne à 1. et on invalide la position actuelle de la pièce n-1.

# Initialisation
monCube = Cube()
listePieces = monCube.initListePieces()
listeSens = monCube.initListeSens()

def resoudre (monCube, piece):
    if piece.position == 'init':
        # Trouver la permière position vide
        flag = False
        for z in range(0, monCube.size):
            for y in range(0, monCube.size):
                for x in range(0, monCube.size):
                    if monCube.cube[x][y][z] == 0:
                        print("[x: "+ str(x) + ", y: " + str(y) + ", z: " + str(z) + "]")
                        piece.position = [x,y,z]
                        flag = True
                    else:
                        continue
                    if flag:
                        break
                if flag:
                    break
            if flag:
                break

        for sens in listeSens:
            if valide(monCube, piece, sens):
                # On a trouvé une position valide, on s'arrache
                piece.sens = sens
                break

        # SI on sort du for sans position valide, il faut rappeler résoudre pour qu'il trouve une autre position

    else:
        # On avait déjà affecter une position à cette piece mais ça ne marche pas, on passe à la position suivante
        pass

# Cette fonction regarde si une pièce est à un endroit valide du cube
def valide (monCube, piece, sens):
    # Return True ou False
    pass

for piece in listePieces:
    print("Piece : " + piece.name)
    resoudre(monCube, piece)
    print("Position : " + str(piece.position) + "\n\n")

print(monCube.cube)
