from .piece import Piece
from .sens import Sens

class Cube:

    def __init__(self):
        self.size = 5
        self.cube = [[[0 for x in range(self.size)] for y in range(self.size)] for z in range(self.size)]

    def afficher(self):
        print(self.cube)

    def placerPiece(self, pieceX):
        print("Je place la pièce " + pieceX.name)

    def initListePieces(self):
        # On attribut une lettre de A à Y pour chaque pièce pour pouvoir les identifier
        listePieces = []
        for lettre in 'ABCDEFGHIJKLMNOPQRSTUVWXY':
            listePieces.append(Piece(lettre, 'init', 'init'))

        return listePieces;

    def initListeSens(self):
        listeSens = []
        # On créé les 24 sens possible pour une pièce à une position donnée avec les coordonées de chaque cube au format [x, y, z]
        # Les 4 premiers cubes sont la "ligne" et le 5ème est le bout qui dépasse

        ## 8 cas de rotation autour de l'axe x :
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [2,1,0] # Le bout est sur le "3ème" bloc en x, en direction des y positifs
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [2,0,1] # Le bout est sur le "3ème" bloc en x, en direction des z positifs
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [2,-1,0] # Le bout est sur le "3ème" bloc en x, en direction des y négatifs
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [2,0,-1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [1,1,0] # Le bout est sur le "2ème" bloc en x, en direction des y positifs
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [1,0,1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [1,-1,0]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [1,0,0],
            [2,0,0],
            [3,0,0],
            [1,0,-1]
            ))

        ## 8 cas de rotation autour de l'axe y :
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [1,2,0]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [0,2,1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [-1,2,0]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [0,2,-1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [1,1,0]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [0,1,1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [-1,1,0]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,1,0],
            [0,2,0],
            [0,3,0],
            [0,1,-1]
            ))

        ## 8 cas de rotation autour de l'axe z :
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [1,0,2]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [0,1,2]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [-1,0,2]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [0,-1,2]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [1,0,1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [0,1,1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [-1,0,1]
            ))
        listeSens.append(Sens(
            [0,0,0],
            [0,0,1],
            [0,0,2],
            [0,0,3],
            [0,-1,1]
            ))

        return listeSens;
