class Piece:

    def __init__(self, name, position, sens):
        self.name = name
        self.position = position
        self.sens = sens

    def afficher(self):
        print("Je suis la pièce " + self.name +".")
        print("Position : " + self.position +".")
        print("Sens " + self.sens +".")
